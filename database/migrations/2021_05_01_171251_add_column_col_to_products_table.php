<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnColToProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->tinyInteger('condition')->default(1)->comment('Tình trạng xe 1=>  mới | 0=>cũ');
            $table->integer('used')->default(0)->comment('Số năm sử dụng xe');
            $table->integer('kilometer')->default(0)->comment('Số km đã đi');
            $table->bigInteger('views')->default(0)->comment('Lượt xem');
            $table->string('type')->nullable()->comment('Kiểu xe: xe số, xe ga,...');
            $table->string('year')->nullable()->comment('Năm sản xuất');
            $table->string('fuel')->nullable()->comment('Nhiên liệu');
            $table->string('engine')->nullable()->comment('Động cơ/Phân khối');
            $table->string('color')->nullable()->comment('Màu chủ đạo');
            $table->string('address')->nullable()->comment('Khu vực');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('products', function (Blueprint $table) {
            $table->dropColumn('condition');
            $table->dropColumn('used');
            $table->dropColumn('kilometer');
            $table->dropColumn('views');
            $table->dropColumn('type');
            $table->dropColumn('year');
            $table->dropColumn('fuel');
            $table->dropColumn('engine');
            $table->dropColumn('color');
            $table->dropColumn('address');
        });
    }
}

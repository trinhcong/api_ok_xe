<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBillsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bills', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('email')->nullable();
            $table->integer('phone');
            $table->bigInteger('member_id');
            $table->bigInteger('price')->default(0)->comment("Tổng giá của đơn hàng");
            $table->text('address')->nullable()->comment("Địa chỉ nhận hàng");
            $table->text('content')->nullable()->comment("Nội dung ghi chú cho đơn hàng");
            $table->tinyInteger('status')->default(0)->comment("0=>Chờ xử lý | 1=>Đã xử lý");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bills');
    }
}

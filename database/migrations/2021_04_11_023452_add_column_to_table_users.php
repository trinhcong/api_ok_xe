<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnToTableUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('cover_image')->nullable()->comment("Ảnh bìa");
            $table->text('description')->nullable();
            $table->text('address')->nullable();
            $table->integer('total_product')->default(0)->comment("số sản phẩm đanng bán");
            $table->integer('followers')->default(0)->comment("Người theo dõi");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('cover_image');
            $table->dropColumn('description');
            $table->dropColumn('total_product');
            $table->dropColumn('followers');
        });
    }
}

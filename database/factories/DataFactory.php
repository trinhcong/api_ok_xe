<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */


use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(\App\Entities\Bank::class, function (Faker $faker) {
    $branches = ["Hà Nội", "Bắc Ninh", "Hồ Chí Minh", "Bắc Giang", "Hải Dương"];
    $random_keys = array_rand($branches);
    return [
        'name' => 'Ngân hàng ' . $branches[$random_keys],
        'number' => random_int(10000000, 999999999),
        'branch' => $branches[$random_keys],
        'status' => 1,
        'created_at' => \Carbon\Carbon::now(),
        'updated_at' => \Carbon\Carbon::now(),
    ];
});
$factory->define(\App\Entities\Banner::class, function (Faker $faker) {
    $number = random_int(1, 1000);
    return [
        'name' => 'Ảnh' . $faker->username,
        'image' => "/storage/banners/" . random_int(1000, 9000) . 'jpg',
        'created_at' => \Carbon\Carbon::now(),
        'updated_at' => \Carbon\Carbon::now(),
    ];
});
$factory->define(\App\Entities\Product::class, function (Faker $faker) {
    return [
        'code' => random_int(1000, 9000),
        'name' => "product " . random_int(1000, 9000),
        'slug' => Str::slug("product " . random_int(1000, 9000) . '-' . random_int(1000, 9000)),
        'description' => "Mô tả " . random_int(1000, 9000),
        'content' => "Nội dung " . random_int(1000, 9000),
        'image' => "/storage/products/" . random_int(1000, 9000) . 'jpg',
        'images' => "[\"\\/storage\\/products\\/gdUi1VzDkAxArwVRN1TYhoPxHvxw7PejM80CY3Rx.jpg\",\"\\/storage\\/products\\/9YpYHPebNX8azYgsRJO0DwlKdGWz8kKI6ytWJxT2.jpg\"]",
        'price' => 1000000,
        'brand_id' => random_int(1, 20),
        'status' => 1,
        'created_at' => \Carbon\Carbon::now(),
        'updated_at' => \Carbon\Carbon::now(),
    ];
});
$factory->define(\App\Entities\Brand::class, function (Faker $faker) {
    return [
        'name' => 'Thương hiệu ' . random_int(1000, 9000),
        'image' => "/storage/brands/" . random_int(1000, 9000) . 'jpg',
        'status' => 1,
        'created_at' => \Carbon\Carbon::now(),
        'updated_at' => \Carbon\Carbon::now(),
    ];
});
$factory->define(\App\Entities\Category::class, function (Faker $faker) {
    return [
        'name' => 'Chuyên mục ' . random_int(1000, 9000),
        'image' => "/storage/categories/" . random_int(1000, 9000) . 'jpg',
        'status' => 1,
        'type' => random_int(1, 2),
        'created_at' => \Carbon\Carbon::now(),
        'updated_at' => \Carbon\Carbon::now(),
    ];
});
$factory->define(\App\Entities\Contact::class, function (Faker $faker) {
    return [
        'member_id' => random_int(1, 20),
        'name' => "Câu hỏi " . random_int(1, 20),
        'email' => $faker->unique()->safeEmail,
        'phone' => random_int(100000000, 999999999),
        'content' => "Nội dung " . random_int(1, 20),
        'status' => 1,
        'created_at' => \Carbon\Carbon::now(),
        'updated_at' => \Carbon\Carbon::now(),
    ];
});
$factory->define(\App\Entities\Member::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'otp' => random_int(100000,999999),
        'avatar' => "/storage/avatar/" . random_int(1000, 9000) . 'jpg',
        'cover_image' => "/storage/cover_image/" . random_int(1000, 9000) . 'jpg',
        'description' => "Mô tả ".random_int(1,20),
        'address' => "Địa chỉ ".random_int(1,20),
        'status' => 1,
        'email' => $faker->unique()->safeEmail,
        'email_verified_at' => now(),
        'password' => bcrypt(11111111), // password
        'remember_token' => Str::random(10),
        'created_at' => \Carbon\Carbon::now(),
        'updated_at' => \Carbon\Carbon::now(),
    ];
});
$factory->define(\App\Entities\Notification::class, function (Faker $faker) {
    return [
        'post_id' => random_int(1, 20),
        'product_id' => random_int(1, 20),
        'status' => 1,
        'created_at' => \Carbon\Carbon::now(),
        'updated_at' => \Carbon\Carbon::now(),
    ];
});
$factory->define(\App\Entities\Page::class, function (Faker $faker) {
    return [
        'name' => "Page " . random_int(1, 20),
        'slug' => Str::slug("Page " . random_int(1, 20) . random_int(1000, 9000)),
        'content' => "Nội dung page ".random_int(1, 20),
        'status' => 1,
        'created_at' => \Carbon\Carbon::now(),
        'updated_at' => \Carbon\Carbon::now(),
    ];
});
$factory->define(\App\Entities\Post::class, function (Faker $faker) {
    return [
        'name' => "Post " . random_int(1, 20),
        'slug' => Str::slug("Post " . random_int(1, 20) . random_int(1000, 9000)),
        'image' => "/storage/posts/" . random_int(1, 20) . 'jpg',
        'description' => "Mô tả " . random_int(1, 20),
        'views' => random_int(1000, 9000),
        'content' => "Nội dung Post ".random_int(1, 20),
        'status' => 1,
        'created_at' => \Carbon\Carbon::now(),
        'updated_at' => \Carbon\Carbon::now(),
    ];
});

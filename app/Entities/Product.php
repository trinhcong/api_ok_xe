<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Product.
 *
 * @package namespace App\Entities;
 */
class Product extends Model implements Transformable
{
    use TransformableTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];
    public function categories(){
        return $this->belongsToMany(Category::class,'product_category');
    }
    public function bills(){
        return $this->hasMany(Bill::class);
    }
    public function brand(){
        return $this->belongsTo(Brand::class);
    }
}
